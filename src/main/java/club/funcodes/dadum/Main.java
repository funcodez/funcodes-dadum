// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.dadum;

import static org.refcodes.cli.CliSugar.*;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.security.SecureRandom;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.MemoryUnit;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.numerical.NumericalUtility;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.RandomTextMode;
import org.refcodes.textual.VerboseTextBuilder;

import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "dadum";
	private static final String TITLE = "n.." + NAME.toUpperCase() + "..∞";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/dadum_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.SANS_SERIF, FontStyle.PLAIN );
	private static final String DESCRIPTION = "Tool for dumping mass data (\"[da]ta-[dum]per\") by pattern to files or streams (see [https://www.metacodes.pro/manpages/dadum_manpage]).";
	private static final String BYTES_PROPERTY = "bytes";
	private static final String TEXT_PROPERTY = "text";
	private static final String OUTPUTFILE_PROPERTY = "outputFile";
	private static final String RND_BYTES_PROPERTY = "rndBytes";
	private static final String RND_TEXT_PROPERTY = "rndText";
	private static final String RND_TEXT_MODE_PROPERTY = "rndTextMode";
	private static final String SIZE_UNIT_PROPERTY = "sizeUnit";
	private static final String SIZE_PROPERTY = "size";

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final EnumOption<RandomTextMode> theRndTextModeArg = enumOption( "rnd-text-mode", RandomTextMode.class, RND_TEXT_MODE_PROPERTY, "The random text mode for generating random text output: " + VerboseTextBuilder.asString( RandomTextMode.values() ) );
		final EnumOption<MemoryUnit> theSizeUnitArg = enumOption( "unit", MemoryUnit.class, SIZE_UNIT_PROPERTY, "The memory unit to use when calculating the size: " + VerboseTextBuilder.asString( MemoryUnit.values() ) );
		final Flag theRndTextFlag = flag( "rnd-text", RND_TEXT_PROPERTY, "Use random text for generating text output." );
		final Flag theRndBytesFlag = flag( "rnd-bytes", RND_BYTES_PROPERTY, "Use random bytes for generating binary output." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", OUTPUTFILE_PROPERTY, "The output file which to process to." );
		final StringOption theTextArg = stringOption( 't', "text", TEXT_PROPERTY, "The text message which to process." );
		final StringOption theBytesArg = stringOption( 'b', "bytes", BYTES_PROPERTY, "The message in bytes (e.g. \"127, 128, 0x10, 0xFF\") which to process." );
		final StringOption theSizeArg = stringOption( 's', "size", SIZE_PROPERTY, "The size of the data to be dumped (can be an expression such as \"640 * 480\", a value of '∞' or `-1` will bomb your storage)." );
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( 
				theSizeArg, any( xor( theRndBytesFlag, and ( theRndTextFlag, any( theRndTextModeArg ) ), theBytesArg, theTextArg ) ), any( and( theOutputFileArg, any( theVerboseFlag ) ), theSizeUnitArg )   
			),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Dump random binary data to a file", theSizeArg, theRndBytesFlag, theOutputFileArg, theVerboseFlag ), 
			example( "Dump random text data to a file", theSizeArg,theRndTextFlag, theOutputFileArg, theVerboseFlag ),
			example( "Dump random text data using specified charset to a file", theSizeArg,theRndTextFlag, theRndTextModeArg, theOutputFileArg, theVerboseFlag ),
			example( "Dump binary data using specified bytes (repeatedly) to a file", theSizeArg, theBytesArg, theOutputFileArg, theVerboseFlag ),
			example( "Dump text data using specified text (repeatedly) to a file", theSizeArg, theTextArg, theOutputFileArg, theVerboseFlag ),
			example( "Dump random binary data to STDOUT", theSizeArg, theRndBytesFlag, theVerboseFlag ), 
			example( "Dump random text data to STDOUT", theSizeArg,theRndTextFlag, theVerboseFlag ),
			example( "Dump random text data using specified charset to STDOUT", theSizeArg,theRndTextFlag, theRndTextModeArg, theVerboseFlag ),
			example( "Dump binary data using specified bytes (repeatedly) to STDOUT", theSizeArg, theBytesArg, theVerboseFlag ),
			example( "Dump text data using specified text (repeatedly) to STDOUT", theSizeArg, theTextArg, theVerboseFlag ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theVerboseFlag.isEnabled();

		try {
			final String theText = theArgsProperties.get( theTextArg );
			final String theBytesString = theArgsProperties.get( theBytesArg );
			final String theOutputFileName = theArgsProperties.get( theOutputFileArg );
			final boolean isRndBytes = theArgsProperties.getBoolean( theRndBytesFlag );
			final boolean isRndText = theArgsProperties.getBoolean( theRndTextFlag );
			final MemoryUnit theSizeUnit = theArgsProperties.getEnumOr( MemoryUnit.class, theSizeUnitArg, MemoryUnit.BYTE );
			byte[] theBytes = theBytesString != null ? NumericalUtility.toBytes( theBytesString ) : null;
			String theSizeText = theArgsProperties.get( theSizeArg );
			if ( "∞".equals( theSizeText ) ) {
				theSizeText = "-1";
			}
			final long theSize = (long) new ExpressionBuilder( theSizeText ).build().evaluate();
			final long theBytesSize = theSizeUnit.toBytes( theSize ).longValue();
			final MemoryUnit theSuitableSizeUnit = MemoryUnit.toSuitableUnit( theBytesSize );
			final RandomTextMode theRndTextMode = isRndText ? RandomTextMode.ALPHANUMERIC : theArgsProperties.getEnum( RandomTextMode.class, theRndTextModeArg );
			if ( isVerbose ) {
				if ( theText != null && theText.length() != 0 ) {
					LOGGER.info( "Text = " + theText );
				}
				if ( theBytes != null && theBytes.length != 0 ) {
					LOGGER.info( "Bytes = { " + NumericalUtility.toHexString( ", ", theBytes ) + " }" );
				}
				if ( theRndTextMode != null ) {
					LOGGER.info( "Encoding = " + theRndTextMode );
				}
				LOGGER.info( "Size = " + theSize + " " + ( theSizeUnit.getUnit() + ( theSizeUnit != theSuitableSizeUnit ? " (" + theSuitableSizeUnit.fromBytes( theBytesSize ) + " " + theSuitableSizeUnit.getUnit() + ")" : "" ) ) + ( ( theSizeUnit != MemoryUnit.BYTE && theSuitableSizeUnit != MemoryUnit.BYTE ) ? " [" + theBytesSize + " " + MemoryUnit.BYTE.getUnit() + "]" : "" ) );
			}
			OutputStream theOutputStream = Execution.toBootstrapStandardOut();
			if ( theOutputFileName != null && theOutputFileName.length() != 0 ) {
				final File theOutputFile = new File( theOutputFileName );
				if ( isVerbose ) {
					LOGGER.info( "Output file = \"" + theOutputFileName + "\" (<" + theOutputFile.getAbsolutePath() + ">)" );
				}
				theOutputStream = new FileOutputStream( theOutputFile );
			}
			try ( OutputStream theBufferedOutputStream = new BufferedOutputStream( theOutputStream ) ) {

				if ( theText != null && theText.length() != 0 ) {
					theBytes = theText.getBytes();
				}

				long theCount = 0;
				final SecureRandom theRnd = new SecureRandom();
				int eRnd;
				while ( theCount < theBytesSize || theBytesSize == -1 ) {
					if ( theRndTextMode != null ) {
						eRnd = theRnd.nextInt( theRndTextMode.getCharSet().length );
						theBufferedOutputStream.write( theRndTextMode.getCharSet()[eRnd] );
						theCount++;
					}
					else if ( isRndBytes ) {
						eRnd = theRnd.nextInt( 256 );
						theBufferedOutputStream.write( eRnd );
						theCount++;
					}
					else if ( theBytes != null && theBytes.length != 0 ) {
						if ( theCount + theBytes.length <= theBytesSize || theBytesSize == -1 ) {
							theBufferedOutputStream.write( theBytes );
							theCount += theBytes.length;
						}
						else {
							for ( int i = 0; i < theBytes.length; i++ ) {
								if ( theCount < theBytesSize ) {
									theBufferedOutputStream.write( theBytes[i] );
									theCount++;
								}
								else {
									break;
								}
							}
						}
					}
					else {
						eRnd = theRnd.nextInt( RandomTextMode.ALPHANUMERIC.getCharSet().length );
						theBufferedOutputStream.write( RandomTextMode.ALPHANUMERIC.getCharSet()[eRnd] );
						theCount++;
					}
				}
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}
}
